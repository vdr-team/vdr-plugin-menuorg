# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Vladimír Bárta <vladimir.barta@k2atmitec.cz>, 2006, 2008
# Jiří Dobrý <jdobry@centrum.cz>, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: menuorg\n"
"Report-Msgid-Bugs-To: <see README>\n"
"POT-Creation-Date: 2017-06-09 19:20+0200\n"
"PO-Revision-Date: 2011-02-16 14:25+0100\n"
"Last-Translator: Milan Hrala <hrala.milan@gmail.com>\n"
"Language-Team: Slovak <hrala.milan@gmail.com>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Reorganizes the main menu"
msgstr "Reorganizácia hlavného menu"

msgid "Failed to load MenuOrg's config file!"
msgstr "Chyba pri načítaní konfiguračného súboru!"

msgid "Enable custom menu"
msgstr "Povoliť vlastné menu"

msgid "Include unconfigured plugins"
msgstr "Vložiť nenakonfigurované moduly"
