vdr-plugin-menuorg (0.5.2-3) experimental; urgency=medium

  * Build-depend on vdr 2.6.0 and update standards version to 4.6.0
  * Remove Thomas Günther from uploaders

 -- Tobias Grimm <etobi@debian.org>  Thu, 13 Jan 2022 19:57:54 +0100

vdr-plugin-menuorg (0.5.2-2) experimental; urgency=medium

  * Update debhleper-compat to 13
  * Build-Depend on vdr >= 2.4.7

 -- Tobias Grimm <etobi@debian.org>  Mon, 20 Dec 2021 19:29:32 +0100

vdr-plugin-menuorg (0.5.2-1) experimental; urgency=medium

  * New upstream version 0.5.2
  * VCS moved to salsa.debian.org
  * Updated debian/watch to point to git repository
  * Build-depend on vdr >= 2.4.1 and updated debian/rules
  * Standards-Version: 4.4.0
  * Update to debhelper 12
  * Whitespace fixed in debian/changelog
  * Added source format
  * Replaced priority extra with optional

 -- Tobias Grimm <etobi@debian.org>  Thu, 12 Sep 2019 09:00:18 +0200

vdr-plugin-menuorg (0.5.1-4) experimental; urgency=medium

  * Build-depend on vdr-dev (>= 2.1.9)

 -- Tobias Grimm <etobi@debian.org>  Tue, 10 Feb 2015 19:14:59 +0100

vdr-plugin-menuorg (0.5.1-3) experimental; urgency=low

  * Build-depend on vdr-dev (>= 2.0.0)

 -- Tobias Grimm <etobi@debian.org>  Sun, 31 Mar 2013 13:59:02 +0200

vdr-plugin-menuorg (0.5.1-2) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.7.42)

 -- Tobias Grimm <etobi@debian.org>  Sat, 23 Mar 2013 20:52:09 +0100

vdr-plugin-menuorg (0.5.1-1) experimental; urgency=low

  * New upstream release
  * Build-depend on vdr-dev (>= 1.7.41)

 -- Tobias Grimm <etobi@debian.org>  Sun, 17 Mar 2013 21:27:33 +0100

vdr-plugin-menuorg (0.5.0-1) experimental; urgency=low

  * New upstream release
  * Using debhelper 9
  * Updated debian/copyright
  * Build-depend on vdr-dev (>= 1.7.40)
  * Standards-Version: 3.9.4

 -- Tobias Grimm <etobi@debian.org>  Sat, 16 Mar 2013 18:22:15 +0100

vdr-plugin-menuorg (0.4.5-1) experimental; urgency=low

  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Sun, 05 Feb 2012 19:58:47 +0100

vdr-plugin-menuorg (0.4.4-4) experimental; urgency=low

  * Switched to GIT using pristine tar

 -- Tobias Grimm <etobi@debian.org>  Sat, 29 Oct 2011 19:51:08 +0200

vdr-plugin-menuorg (0.4.4-3) experimental; urgency=low

  * Removed non-standard shebang line from debian/rules
  * Standards-Version: 3.8.3

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Nov 2009 19:36:25 +0100

vdr-plugin-menuorg (0.4.4-2) experimental; urgency=low

  * Release for vdrdevel 1.7.6
  * Changed Maintainer to Debian VDR Team
  * Added Tobias Grimm and myself to Uploaders
  * Updated debian/copyright to our new format
  * Added ${misc:Depends}
  * Bumped standards version to 3.8.1
  * Changed section to "video"

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 29 Apr 2009 23:10:57 +0200

vdr-plugin-menuorg (0.4.4-1) experimental; urgency=low

  * New upstream release
  * New vdr dependency handling via virtual vdr-abi-* provided by
    vdr, build-depend on vdr-dev (>= 1.6.0-5)
  * Bumped Standards-Version to 3.8.0

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 25 Jul 2008 19:25:39 +0200

vdr-plugin-menuorg (0.4.3-2) experimental; urgency=low

  * Increased package version to force rebuild for vdr 1.6.0-1ctvdr7

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 12 May 2008 13:00:40 +0200

vdr-plugin-menuorg (0.4.3-1) experimental; urgency=low

  * New upstream release
  * Build-depend on vdr-dev (>=1.6.0)
  * Bumped Standards-Version to 3.7.3
  * Switched Build-System to cdbs, Build-Depend on cdbs
  * Updated debian/copyright
  * Link menuorg.xml to /etc/vdr/plugins/menuorg.xml

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 03 Apr 2008 23:47:32 +0200

vdr-plugin-menuorg (0.3.1-3) experimental; urgency=low

  * Force rebuild for vdr 1.5.15

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 18 Feb 2008 21:08:17 +0100

vdr-plugin-menuorg (0.3.1-2) experimental; urgency=low

  * Force update for new VDR release

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 16 Feb 2008 21:52:57 +0100

vdr-plugin-menuorg (0.3.1-1) unstable; urgency=low

  * New upstream release
  * Removed 90_menuorg-0.3-1.5.9.dpatch

 -- Tobias Grimm <tg@e-tobi.net>  Tue, 11 Dec 2007 01:26:57 +0100

vdr-plugin-menuorg (0.3-7) unstable; urgency=low

  * Fixed 90_menuorg-0.3-1.5.9.dpatch to build with VDR 1.4 as well

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 26 Nov 2007 21:17:41 +0100

vdr-plugin-menuorg (0.3-6) unstable; urgency=low

  * Release for vdrdevel 1.5.12

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 20 Nov 2007 23:46:21 +0100

vdr-plugin-menuorg (0.3-5) unstable; urgency=low

  * Release for vdrdevel 1.5.11

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  6 Nov 2007 23:34:14 +0100

vdr-plugin-menuorg (0.3-4) unstable; urgency=low

  * Release for vdrdevel 1.5.10

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 16 Oct 2007 23:50:55 +0200

vdr-plugin-menuorg (0.3-3) unstable; urgency=low

  * Added 90_menuorg-0.3-1.5.9.dpatch

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 29 Aug 2007 01:44:00 +0200

vdr-plugin-menuorg (0.3-2) unstable; urgency=low

  * Release for vdrdevel 1.5.9

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 28 Aug 2007 01:01:06 +0200

vdr-plugin-menuorg (0.3-1) unstable; urgency=low

  * New upstream release

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 25 Aug 2007 21:48:43 +0200

vdr-plugin-menuorg (0.2-1) unstable; urgency=low

  * New upstream release (fixes issue with burn plug-in)

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 25 Aug 2007 20:26:09 +0200

vdr-plugin-menuorg (0.1-1) unstable; urgency=low

  * Initial release

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 24 Aug 2007 21:27:05 +0200
